# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql.aggregate import Max
from sql.conditionals import Coalesce
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['InvoiceLine']


class InvoiceLine:
    __name__ = 'account.invoice.line'
    __metaclass__ = PoolMeta

    last_prices = fields.Function(
        fields.Many2Many('account.invoice.line', None, None, 'Last prices',
            domain=[('product', '=', Eval('product'))],
            depends=['product']),
        'get_last_prices')

    @fields.depends('product', 'invoice', 'stock_moves')
    def on_change_with_last_prices(self):
        return self.get_last_prices([self])[self.id]

    @classmethod
    def get_last_prices(cls, records, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLineMove = pool.get('account.invoice.line-stock.move')
        Move = pool.get('stock.move')
        invoice = Invoice.__table__()
        line = cls.__table__()
        invoice_move = InvoiceLineMove.__table__()
        move = Move.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: [] for r in records}
        for record in records:
            if not record.invoice:
                continue
            max_date = None
            if record.stock_moves:
                max_date = max(m.effective_date or m.planned_date
                    for m in record.stock_moves)
                if not max_date:
                    max_date = record.invoice.invoice_date
            if not max_date:
                continue
            cursor.execute(*line.join(
                invoice, condition=(invoice.id == line.invoice)
            ).join(invoice_move, condition=(
                invoice_move.invoice_line == line.id)
            ).join(move, condition=(move.id == invoice_move.stock_move)
            ).select(Max(line.id),
                 Max(Coalesce(move.effective_date, move.planned_date)),
                 where=((line.type == 'line') &
                        (invoice.id != record.invoice.id) &
                        (line.product == record.product.id) &
                        (invoice.party == record.invoice.party.id
                            if record.invoice else record.party.id) &
                        (invoice.state == 'posted') &
                        (invoice.type == record.invoice_type) &
                        (Coalesce(Coalesce(move.effective_date,
                            move.planned_date), invoice.invoice_date) <=
                            max_date)),
                 order_by=(line.product, line.invoice),
                 group_by=(line.product, line.invoice),
                 limit=3)
            )
            res[record.id] = [row[0] for row in cursor.fetchall()] or []
        return res
