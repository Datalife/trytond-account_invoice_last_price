# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .invoice import InvoiceLine
from .stock import Move


def register():
    Pool.register(
        InvoiceLine,
        Move,
        module='account_invoice_last_price', type_='model')
